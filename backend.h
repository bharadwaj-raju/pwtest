/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#pragma once

#include <QDBusArgument>
#include <QDBusConnection>
#include <QDBusConnectionInterface>
#include <QDBusObjectPath>
#include <QDBusUnixFileDescriptor>
#include <QDebug>

#include <gst/gst.h>

#include <pipewire/pipewire.h>
#include <spa/param/format-utils.h>
#include <spa/param/video/format-utils.h>
#include <spa/param/props.h>
#include <spa/param/video/raw.h>
#include <spa/utils/result.h>

struct data {
    struct pw_context *context;
    struct pw_core *core;
    struct pw_thread_loop *thread_loop;
    struct pw_stream *stream;

    struct spa_video_info_raw format;
};

class Backend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool readyToStream READ readyToStream NOTIFY readyToStreamChanged)

public:
    explicit Backend(QObject *parent = nullptr);
    Q_INVOKABLE void requestScreenSharing();
    Q_INVOKABLE void stopRecording();
    Q_INVOKABLE void startRecording();
    bool readyToStream();
    typedef struct {
        uint node_id;
        QVariantMap map;
    } Stream;
    typedef QList<Stream> Streams;

signals:
    void readyToStreamChanged();

private:
    bool m_readyToStream = false;
    Streams m_streams;
    QString m_portalDBusName = "org.freedesktop.portal.Desktop";
    QString m_portalDBusIFace = "org.freedesktop.portal.ScreenCast";
    uint m_sessionToken = 0;
    uint m_requestToken = 0;
    QString m_sessionHdl;
    struct data data = { 0 };
    Q_SLOT void sessionCreated(uint response, const QVariantMap &results);
    Q_SLOT void sourcesSelected();
    Q_SLOT void screenCastStarted(uint response, const QVariantMap &results);
};
