/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "backend.h"
#include "stdio.h"

Q_DECLARE_METATYPE(Backend::Stream);
Q_DECLARE_METATYPE(Backend::Streams);

#include <stdio.h>

Backend::Backend(QObject *parent)
    : QObject(parent)
{
    pw_init(nullptr, nullptr);

    data.thread_loop = pw_thread_loop_new("pw-thread-loop", NULL);
//     pw_thread_loop_lock(data.thread_loop);

    data.context = pw_context_new(pw_thread_loop_get_loop(data.thread_loop), NULL, sizeof(GstElement*));
    data.core = pw_context_connect(data.context, NULL, sizeof(GstElement*));

//     if (pw_thread_loop_start(data.thread_loop) < 0) {
//         return;
//     }

    gst_init(nullptr, nullptr);
//     pw_thread_loop_unlock(data.thread_loop);

}

const QDBusArgument &operator>>(const QDBusArgument &arg, Backend::Stream &stream)
{
    arg.beginStructure();
    arg >> stream.node_id;

    arg.beginMap();
    while (!arg.atEnd()) {
        QString key;
        QVariant map;
        arg.beginMapEntry();
        arg >> key >> map;
        arg.endMapEntry();
        stream.map.insert(key, map);
    }
    arg.endMap();
    arg.endStructure();

    return arg;
}

bool Backend::readyToStream()
{
    return m_readyToStream;
}

void Backend::requestScreenSharing()
{
    QDBusMessage message =
        QDBusMessage::createMethodCall(m_portalDBusName, QLatin1String("/org/freedesktop/portal/desktop"), m_portalDBusIFace, QLatin1String("CreateSession"));
    m_sessionToken++;
    m_requestToken++;
    message << QVariantMap{{QLatin1String("session_handle_token"), QString("u%1").arg(m_sessionToken)},
                           {QLatin1String("handle_token"), QString("u%1").arg(m_requestToken)}};

    QDBusMessage reply = QDBusConnection::sessionBus().call(message);
    if (reply.type() == QDBusMessage::ErrorMessage && m_portalDBusName == QStringLiteral("org.freedesktop.portal.Desktop")) {
        /* Workaround: Neon's (Ubuntu LTS) build of xdg-desktop-portal isn't built
         * with PipeWire support so it doesn't have the ScreenCast interface even
         * though the platform implementation may support it. To make this work on
         * KDE Neon, we'll instead use the platform implementation directly */
        qWarning() << "Trying workaround for KDE Neon...";
        m_portalDBusName = "org.freedesktop.impl.portal.desktop.kde";
        m_portalDBusIFace = "org.freedesktop.impl.portal.ScreenCast";
        requestScreenSharing();
        return;
    }
    QDBusConnection::sessionBus().connect(QString(),
                                          reply.arguments()[0].value<QDBusObjectPath>().path(),
                                          QLatin1String("org.freedesktop.portal.Request"),
                                          QLatin1String("Response"),
                                          this,
                                          SLOT(sessionCreated(uint, QVariantMap)));
}

void Backend::sessionCreated(uint response, const QVariantMap &results)
{
    if (response != 0) {
        qWarning() << "Failed to create session: " << response;
        return;
    }

    QDBusMessage message =
        QDBusMessage::createMethodCall(m_portalDBusName, QLatin1String("/org/freedesktop/portal/desktop"), m_portalDBusIFace, QLatin1String("SelectSources"));

    m_sessionHdl = results.value(QLatin1String("session_handle")).toString();
    m_requestToken++;
    message << QVariant::fromValue(QDBusObjectPath(m_sessionHdl))
            << QVariantMap{{QLatin1String("multiple"), false},
                           // right now window capture support needs some more work -- how to
                           // find width/height?
                           {QLatin1String("types"), (uint)(1 | 2) /* bitmask, 1 = monitor, 2 = window, 1 | 2 = all types */},
                           {QLatin1String("handle_token"), QString("u%1").arg(m_requestToken)}};

    QDBusMessage reply = QDBusConnection::sessionBus().call(message);
    QDBusConnection::sessionBus().connect(QString(),
                                          reply.arguments()[0].value<QDBusObjectPath>().path(),
                                          QLatin1String("org.freedesktop.portal.Request"),
                                          QLatin1String("Response"),
                                          this,
                                          SLOT(sourcesSelected()));
}

void Backend::sourcesSelected()
{
    QDBusMessage message =
        QDBusMessage::createMethodCall(m_portalDBusName, QLatin1String("/org/freedesktop/portal/desktop"), m_portalDBusIFace, QLatin1String("Start"));

    m_requestToken++;
    message << QVariant::fromValue(QDBusObjectPath(m_sessionHdl)) << QString() // parent_window
            << QVariantMap{{QLatin1String("handle_token"), QString("u%1").arg(m_requestToken)}};

    QDBusMessage reply = QDBusConnection::sessionBus().call(message);
    QDBusConnection::sessionBus().connect(QString(),
                                          reply.arguments()[0].value<QDBusObjectPath>().path(),
                                          QLatin1String("org.freedesktop.portal.Request"),
                                          QLatin1String("Response"),
                                          this,
                                          SLOT(screenCastStarted(uint, QVariantMap)));
}

void Backend::screenCastStarted(uint response, const QVariantMap &results)
{
    if (response != 0) {
        m_readyToStream = false;
        Q_EMIT readyToStreamChanged();
        return;
    }

    m_streams = qdbus_cast<Streams>(results.value(QLatin1String("streams")));
    m_readyToStream = true;
    Q_EMIT readyToStreamChanged();
}

static void on_param_changed(void *userdata, uint32_t id, const struct spa_pod* format)
{
    //struct data *data = userdata;

    if (format == NULL || id != SPA_PARAM_Format)
        return;

    struct spa_video_info_raw formatbuf;
    spa_format_video_raw_parse(format, &formatbuf);

    puts("on_param_changed");
    printf("%dx%d\n", formatbuf.size.width, formatbuf.size.height);

    if (!userdata) {
        return;
    }

    g_object_freeze_notify(G_OBJECT(userdata));
//     g_object_set(G_OBJECT(userdata), "height", formatbuf.size.height, NULL);
    g_object_set(G_OBJECT(userdata), "width", formatbuf.size.width, NULL);
    g_object_thaw_notify(G_OBJECT(userdata));
}

static const struct pw_stream_events stream_events = {
    PW_VERSION_STREAM_EVENTS,
    .param_changed = on_param_changed,
};


void Backend::startRecording()
{
    for (Stream stream : m_streams) {
        QDBusMessage message = QDBusMessage::createMethodCall(m_portalDBusName,
                                                              QLatin1String("/org/freedesktop/portal/desktop"),
                                                              m_portalDBusIFace,
                                                              QLatin1String("OpenPipeWireRemote"));

        message << QVariant::fromValue(QDBusObjectPath(m_sessionHdl)) << QVariantMap();

        QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
        pendingCall.waitForFinished();
        QDBusPendingReply<QDBusUnixFileDescriptor> reply = pendingCall.reply();
        if (reply.isError()) {
            qDebug() << "Couldn't open PW remote";
        }
        qDebug() << "fd" << reply.value().fileDescriptor();
        qDebug() << "path" << stream.node_id;

        const struct spa_pod *params[2];
        uint8_t buffer[1024];
        struct spa_pod_builder b = SPA_POD_BUILDER_INIT(buffer, sizeof(buffer));

//         pw_thread_loop_lock(data.thread_loop);

        pw_properties* reuseProps =
        pw_properties_new_string("pipewire.client.reuse=1");

        data.stream = pw_stream_new(data.core, "screenrecorder-pw-stream", reuseProps);

        spa_rectangle pwMinScreenBounds = spa_rectangle{1, 1};
        spa_rectangle pwMaxScreenBounds = spa_rectangle{UINT32_MAX, UINT32_MAX};

        params[0] = reinterpret_cast<spa_pod*>(spa_pod_builder_add_object(
            &b, SPA_TYPE_OBJECT_Format, SPA_PARAM_EnumFormat,
            SPA_FORMAT_mediaType, SPA_POD_Id(SPA_MEDIA_TYPE_video),
            SPA_FORMAT_mediaSubtype, SPA_POD_Id(SPA_MEDIA_SUBTYPE_raw),
            SPA_FORMAT_VIDEO_format,
            SPA_POD_CHOICE_ENUM_Id(5, SPA_VIDEO_FORMAT_BGRx, SPA_VIDEO_FORMAT_RGBx,
                                    SPA_VIDEO_FORMAT_RGBA, SPA_VIDEO_FORMAT_BGRx,
                                    SPA_VIDEO_FORMAT_BGRA),
            SPA_FORMAT_VIDEO_size,
            SPA_POD_CHOICE_RANGE_Rectangle(&pwMinScreenBounds, &pwMinScreenBounds,
                                            &pwMaxScreenBounds),
            0));

        params[1] = spa_pod_builder_add_object(&b,
            SPA_TYPE_OBJECT_ParamBuffers, SPA_PARAM_Buffers,
            SPA_PARAM_BUFFERS_buffers, SPA_POD_CHOICE_RANGE_Int(0,
                                                                8,
                                                                INT32_MAX),
            SPA_PARAM_BUFFERS_blocks,  SPA_POD_CHOICE_RANGE_Int(0, 1, INT32_MAX),
            SPA_PARAM_BUFFERS_size,    SPA_POD_CHOICE_RANGE_Int(0, 0, INT32_MAX),
            SPA_PARAM_BUFFERS_stride,  SPA_POD_CHOICE_RANGE_Int(0, 0, INT32_MAX),
            SPA_PARAM_BUFFERS_align,   SPA_POD_Int(16),
            SPA_PARAM_BUFFERS_dataType, SPA_POD_CHOICE_FLAGS_Int(
                            1<<SPA_DATA_DmaBuf)
            );

        pw_stream_connect(data.stream,
                          PW_DIRECTION_INPUT,
                          stream.node_id,
                          PW_STREAM_FLAG_AUTOCONNECT,
                          params,
                          2);



        QString muxer = "matroskamux";

        QString gstLaunch = QStringLiteral(
            "pipewiresrc fd=%1 path=%2 do-timestamp=TRUE ! videoparse name=\"videoparse-elem\" width=1440 height=900 format=bgra framerate=%3/1 ! videoconvert ! "
            "x264enc speed-preset=%4 ! %5 ! filesink location=%6")
        .arg(reply.value().fileDescriptor())
        .arg(stream.node_id)
        .arg(30)
        .arg("superfast")
        .arg(muxer)
        .arg("/tmp/out.mkv");

        GstElement *pipeline = gst_parse_launch(gstLaunch.toUtf8(), nullptr);
        GstElement *videoparse_elem = gst_bin_get_by_name(GST_BIN_CAST(pipeline), "videoparse-elem");
        gst_element_set_state(pipeline, GST_STATE_PLAYING);

        spa_hook spa_stream_listener;
//         pw_stream_add_listener(data.stream, &spa_stream_listener, &stream_events, videoparse_elem);

//         pw_thread_loop_unlock(data.thread_loop);

    }
}

void Backend::stopRecording()
{
    m_readyToStream = false;
    Q_EMIT readyToStreamChanged();
    QDBusMessage message =
        QDBusMessage::createMethodCall(m_portalDBusName, m_sessionHdl, QLatin1String("org.freedesktop.portal.Session"), QLatin1String("Close"));
    QDBusPendingCall pendingCall = QDBusConnection::sessionBus().asyncCall(message);
    pendingCall.waitForFinished();
}
