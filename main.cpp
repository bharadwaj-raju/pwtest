/*
  SPDX-FileCopyrightText: 2021 Bharadwaj Raju <bharadwaj.raju777@protonmail.com>

  SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include <iostream>
#include <QApplication>

#include "backend.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    std::cout << "Hello!" << std::endl;
    Backend bk;
    bk.requestScreenSharing();
    bk.connect(
        &bk, &Backend::readyToStreamChanged,
        [&] () {
            if (bk.readyToStream()) {
                bk.startRecording();
            }
        }
    );
    return app.exec();
}

#include "main.moc"
